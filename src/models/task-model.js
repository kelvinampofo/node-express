const mongoose = require('mongoose');

const taskSchema = mongoose.Schema(
	{
		text: {
			type: String,
			required: [true, 'Please add a text field'],
		},
	},
	{
		timestamps: true, // mongoose built-in timestamps
	},
);

module.exports = mongoose.model('Task', taskSchema);
