const asyncHandler = require('express-async-handler');
const Task = require('../models/task-model'); // to use mongoose methods like (.create .find)

// @desc   Create task
// @route  POST /api/tasks
// @access Private
const createTask = asyncHandler(async (req, res) => {
	if (!req.body.text) {
		res.status(400);
		throw new Error('Please add a text field');
	}

	const task = await Task.create({
		text: req.body.text,
	});

	res.status(201).json(task);
});

// @desc   Get tasks
// @route  GET /api/tasks
// @access Private
const getTask = asyncHandler(async (req, res) => {
	const tasks = await Task.find();
	res.status(200).json({ tasks });
});

// @desc   Update tasks
// @route  PUT /api/tasks/:id
// @access Private
const updateTask = asyncHandler(async (req, res) => {
	const task = await Task.findById(req.params.id);

	if (!task) {
		res.status(400);
		throw new Error('Task not found');
	}

	const updatedTask = await Task.findByIdAndUpdate(req.params.id, req.body, {
		new: true, // create task if it doesn't exist
	});

	res.status(200).json(updatedTask);
});

// @desc   Delete tasks
// @route  DELETE /api/tasks/:id
// @access Private
const deleteTask = asyncHandler(async (req, res) => {
	const task = await Task.findById(req.params.id);

	if (!task) {
		res.status(400);
		throw new Error('Task not found');
	}

	await Task.findByIdAndDelete(req.params.id);

	res.status(200).json({ id: req.params.id });
});

module.exports = {
	getTask,
	createTask,
	updateTask,
	deleteTask,
};
