// @desc   Get user
// @route  GET /api/user
// @access Private
const getUser = (req, res) => {
	res.status(200).json({ message: 'User retrieved' });
};

// @desc   Create user
// @route  POST /api/tasks
// @access Private
const registerUser = (req, res) => {
	res.status(201).json({ message: 'User registered' });
};

// @desc   Update user
// @route  PUT /api/user/:id
// @access Private
const updateUser = (req, res) => {
	res.status(200).json({ message: `User ${req.params.id} updated` });
};

// @desc   Delete user
// @route  DELETE /api/user/:id
// @access Private
const deleteUser = (req, res) => {
	res.status(200).json({ message: `User ${req.params.id} deleted` });
};

module.exports = {
	getUser,
	registerUser,
	updateUser,
	deleteUser,
};
