const express = require('express');
const router = express.Router();
const { getTask, createTask, updateTask, deleteTask } = require('../controllers/task-controller');

router.route('/').get(getTask).post(createTask);

router.route('/:id').put(updateTask).delete(deleteTask);

module.exports = router;
