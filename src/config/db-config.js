const mongoose = require('mongoose');

// mongodb functions like (.connect .find .create) return a promise so need to use async await
const connectDB = async () => {
	try {
		const uri = await mongoose.connect(process.env.MONGO_DB_URI);
		console.log(`mongoDB connected on host: ${uri.connection.host}`.cyan.underline);
	} catch (error) {
		console.error(`Error: ${error.message}`);
		process.exit(1); // exit with failure code
	}
};

module.exports = connectDB;
