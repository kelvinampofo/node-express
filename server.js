const express = require('express');
const connectDB = require('./src/config/db-config');
const colors = require('colors');
const dotenv = require('dotenv').config();
const { errorHandler } = require('./src/middleware/error-middleware');
const port = process.env.PORT || '3000';

const app = express();

connectDB();

// Express middleware
app.use(express.json()); // application/json parser
app.use(express.urlencoded({ extended: false })); // application/x-www-form-urlencoded parser

// Routes
app.use('/api/v1/user', require('./src/routes/user-routes'));
app.use('/api/v1/tasks', require('./src/routes/task-routes'));

// Error middleware
app.use(errorHandler);

app.listen(port, () =>
	console.log(`Server started in ${process.env.NODE_ENV} mode on port ${port}`),
);
