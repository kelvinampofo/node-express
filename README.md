# Node-Express API

A starter-kit REST API using Node.js for my backend/full-stack projects. Built with technologies Node, Express, Mongoose/MongoDB & JWT authentication (JSON web token).

## Prerequisites

Please make sure that you have:

1. Node.js installed (https://nodejs.org/)

2. Add dotenv variables to .env file (https://12factor.net/config)

3. Run `npm i` or `yarn` in your root project folder

## Usage

To start server run `npm run server`.
